require 'net/http'
require 'uri'
require 'json'
require 'csv'
# https://f3ca7c14.qb0x.com:31561
# request.basic_auth("e27739906e863b87fb55","f19dfe4b2d")
t=Time.now
max_time=t + 1*60*60    
while t<max_time
  sleep(1)
  uri = URI.parse("https://f3ca7c14.qb0x.com:31561/logstash-*/_search")
  request = Net::HTTP::Get.new(uri)
  request.basic_auth("e27739906e863b87fb55","f19dfe4b2d")
  request.content_type = "application/json"
  

  puts request.body

  top_left_lat = rand(52.2...62.2).round(2)
  top_left_lon = rand(1.2...5.2).round(2)
              
  bot_right_lat = rand(32.2...42.2).round(2)
  bot_right_lon = rand(6.2...10.2).round(2)

  request.body = {
    query: {
      bool: {
        must: {
          match_all: {}
        },
        filter: {
          geo_bounding_box: {
            'geo.coordinates' => {
              top_left: {
                lat: top_left_lat,
                lon: -top_left_lon
              },
              bottom_right: {
                lat: bot_right_lat,
                lon: -bot_right_lon
              }
            },
            type: "indexed"
          }
        }
      }
    }
  }.to_json

  
  req_options = {
    use_ssl: uri.scheme == "https",
  }

  response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
    http.request(request)
  end

  parsed = JSON.parse(response.body)
  puts "#{t} - #{response.code} - #{parsed["took"]} - #{parsed["timed_out"]} - #{parsed["_shards"]["failed"]} - #{parsed["hits"]["total"]}" 
  
  CSV.open("test_es_output/geo_search.csv", "w") do |csv|
    csv << ["#{t}","#{response.code}", "#{parsed["took"]}", "#{parsed["timed_out"]}", "#{parsed["_shards"]["failed"]}", "#{parsed["hits"]["total"]}"]
  end
  t=Time.now
end