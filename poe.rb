require 'net/http'
require 'json'
require 'uri'
require 'csv'
  # *** CHANGE THESE NUMBERS AND ACCOUNT NAME
  tab_number="5"
  account_name="UpsideXV"
  poe_session_id = "a3716aa3e2906a701563300ec3715dd7"


  stash_uri = URI.parse("https://www.pathofexile.com/character-window/get-stash-items?accountName=#{account_name}&league=Legion&tabIndex=#{tab_number}")
  stash_request = Net::HTTP::Get.new(stash_uri)
  stash_request.content_type = "application/json"
  stash_request['Cookie'] = "POESESSID=#{poe_session_id}"

  poe_ninja_uri = URI.parse("https://poe.ninja/api/data/itemoverview?league=Legion&type=BaseType")
  poe_ninja_request = Net::HTTP::Get.new(poe_ninja_uri)
  poe_ninja_request.content_type = "application/json"

  stash_req_options = {
    use_ssl: stash_uri.scheme == "https",
  }

  poe_ninja_req_options = {
    use_ssl: poe_ninja_uri.scheme == "https",
  }

  stash_response = Net::HTTP.start(stash_uri.hostname, stash_req_options) do |http|
    http.request(stash_request)
  end

  poe_ninja_response = Net::HTTP.start(poe_ninja_uri.hostname, poe_ninja_req_options) do |http|
    http.request(poe_ninja_request)
  end

  stash_tab_to_process = JSON.parse(stash_response.body)
  poe_ninja_to_process = JSON.parse(poe_ninja_response.body)

  puts stash_tab_to_process

  stash_tab_to_process["items"].each do |i|
    if i["ilvl"] >= 82 && (i["elder"] || i["shaper"])
      
      if i["elder"]
        variant = "Elder"
      else
        variant = "Shaper"
      end

      poe_ninja_chaos_value = poe_ninja_to_process["lines"].find {|data| i["ilvl"] == data["levelRequired"] && variant == data["variant"] && i["typeLine"] == data["baseType"]}["chaosValue"]
      
      #NOTE: Commented out the trade search, query needs to accept multiple parameters to work as inteded
      # trade_uri = URI.parse("https://www.pathofexile.com/api/trade/search/Legion")
      # trade_request = Net::HTTP::Get.new(trade_uri)
      # trade_request.content_type = "application/json"

      # trade_req_options = {
      #   use_ssl: poe_ninja_uri.scheme == "https"
      # }

      # trade_request.body = {
      #   query:{
      #   filters:{
      #     trade_filters:{
      #       disabled: false,
      #       filters:{
      #         price:{
      #           min:1,
      #           max:30
      #         }
      #     }
      #   }
      # },
      #   status:{option:"online"},
      #   stats:[{type:"and",filters:[]}],
      #   name:"",
      #   type:"Samite Helmet",
      #   sort:{
      #     price:"asc"
      #   }
      # }
      # }.to_json

      # implicit
      # explicit
      # item_armor_type

      # "frameType"      =>3,
      # "category"      =>      {
      #    "armour"         =>         [
      #       "helmet

      # trade_response = Net::HTTP.start(trade_uri.hostname, trade_req_options) do |http|
      #   http.request(trade_request)
      # end

      # trade_response_to_process = JSON.parse(trade_response.body)

      # puts trade_response_to_process.first
      puts "#{poe_ninja_chaos_value},#{i["ilvl"]},#{variant},#{i["typeLine"]}"  
    end


    map_fragments="category->map[fragment]" || fragment_splinter|| fragment_emblem
    incubator{name,currency:incubator}
    scarabs="category->map->[fragment,scarab]"
    fossil{name,currency:fossil}
    essence={name}
    divination_cards(name,catergory:cards)
    prophecies{"descrText"=>"Right-click to add this prophecy to your character."}

    helmet_enchants{"enchantMods"=>["60% increased Lightning Tendrils Critical Strike Chance"]}
    unique_maps
    unique_jewels
    unique_flasks
    unique_weapons
    unique_armours
    unique_accessories

    t=Time.now
    CSV.open("stash_values/stash_value_#{t}.csv", "w") do |csv|
      csv << ["#{poe_ninja_chaos_value}","#{i["ilvl"]}","#{variant}","#{i["typeLine"]}"]
    end
  end